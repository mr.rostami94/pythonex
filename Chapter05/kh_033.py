"""==============================================
منطق برنامه ای که دو عدد را همراه با کدی دریافت می کند و براساس کد
عملیات مورد نظر را انجام می دهد.
=============================================="""

num1 = float(input("Please enter first number: "))
num2 = float(input("Please enter second number: "))
code = input("Please enter code: ")

if code == "A":
    result = num1 + num2
    print("="*20,"\n Result: ",result)
    print("-"*20,"\nGOODBYE")
if code == "S":
    result = num1 - num2
    print("=" * 20, "\n Result: ", result)
    print("-" * 20, "\nGOODBYE")
if code == "M":
    result = num1 * num2
    print("=" * 20, "\n Result: ", result)
    print("-" * 20, "\nGOODBYE")
if code == "D":
    result = num1 / num2
    print("=" * 20, "\n Result: ", result)
    print("-" * 20, "\nGOODBYE")
else:
    print("=" * 20, "\n INVALID CODE ")
    print("-" * 20, "\nGOODBYE")