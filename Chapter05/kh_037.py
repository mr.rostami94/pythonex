"""=======================================================
منطق برنامه ای که براساس مثبت، منفی یا صفر بودن عددی پیام مناسبی را چاپ می کند
======================================================="""

number = float(input("Please enter your number: "))
if number > 0:
    print("="*20,"\nYour number is positive...")
    print("-"*20,"\nGoodbye")
elif number < 0:
    print("=" * 20, "\nYour number is negative...")
    print("-" * 20, "\nGoodbye")
else:
    print("=" * 20, "\nYour number is Zero")
    print("-" * 20, "\nGoodbye")