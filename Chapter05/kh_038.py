"""=======================================================
منطق برنامه ای که براساس وضعیت مجموع اعداد اول و چهارم نسبت به مجموع اعداد
دوم و سوم از چهار عدد ورودی مقدار مشخصی چاپ کند.
======================================================="""

num1 = float(input("Number1: "))
num2 = float(input("Number2: "))
num3 = float(input("Number3: "))
num4 = float(input("Number4: "))

m = num1 + num4
n = num2 + num3

if m >= n:
    print("="*20,"\nResult: ",m)
    print("-"*20,"\nEND...")
else:
    print("=" * 20, "\nResult: ", n)
    print("-" * 20, "\nEND...")