"""===================================
منطق برنامه تعیین کننده بزرگترین عدد مابین سه عدد ورودی
==================================="""
a = int(input("Num1: "))
b = int(input("Num2: "))
c = int(input("Num3: "))
max = a
if b > a:
    max = b
if c > max:
    max = c
print("="*20)
print("Max: ",max)