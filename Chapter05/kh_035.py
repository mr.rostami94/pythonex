"""===============================================================
منطق برنامه ای که دو عدد را دریافت می کند و عدد کوچکتر را به توان عدد بزرگتر می رساند.
==============================================================="""

num1 = float(input("Please enter first number: "))
num2 = float(input("Please enter second number: "))

if num2 <= num1:
    print("="*20,"\nResult: ", num2**num1)
    print("-"*20,"\nGODDBYE")
else:
    print("=" * 20, "\nResult: ", num1 ** num2)
    print("-" * 20, "\nGODDBYE")

