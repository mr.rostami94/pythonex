"""===============================================================
منطق برنامه ای را طراحی کنید که بر اساس وضعیت دو عدد نسبت به هم عملیاتی را انجام می دهد
==============================================================="""

num1 = float(input("First Number: "))
num2 = float(input("Second Number: "))

if num1 < num2:
    p = num1 * num2
    print("="*20,"\nResult: ",p)
    print("-"*20,"\nGOODBYE")
elif num1 > num2:
    p = num1 + num2
    print("=" * 20, "\nResult: ", p)
    print("-" * 20, "\nGOODBYE")
else:
    p = num1
    print("=" * 20, "\nResult: ", p)
    print("-" * 20, "\nGOODBYE")
