"""===============================================
منطق برنامه محاسبه کننده ریشه های معادله درجه دوم ax2 + bx + c = 0
==============================================="""
from math import sqrt
a = int(input("A: "))
b = int(input("B: "))
c = int(input("C: "))
delta = b**2 - (4*a*c)
if delta <0:
    print("=" * 20)
    print("Delta = ",delta)
    print("Delta is negative...")
elif delta == 0:
    print("=" * 20)
    print("Delta = ", delta)
    x = -b / (2*a)
    print("X = ",x)
else:
    print("=" * 20)
    print("Delta = ", delta)
    x1 = (-b + sqrt(delta))/(2*a)
    x2 = (-b - sqrt(delta))/(2*a)
    print("X1: ",x1,"\nX2: ",x2)


