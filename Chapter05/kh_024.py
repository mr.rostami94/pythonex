"""======================================
منطق برنامه تعیین کننده کوچک ترین عدد مابین سه عدد ورودی
======================================"""
a = int(input("Num1:"))
b = int(input("Num2:"))
c = int(input("Num3:"))
min = a
if b < min:
    min = b
if c < min:
    min = c
print("="*20)
print("Min: ",min)