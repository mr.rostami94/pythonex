"""==============================
منطق برنامه تعیین کننده تعداد روزهای یک فصل
=============================="""

name = input("Please enter your name of seasons\n for example: spring,summer,winter,fall ==> ")
if name == "summer":
    days = 3 * 31
    print("=" * 20, "\nDays: ", days)
    print("-" * 20, "\nENG...")
if name == "fall":
    days = 3 * 30
    print("=" * 20, "\nDays: ", days)
    print("-" * 20, "\nENG...")
if name == "winter":
    days = 2 * 30 + 29
    print("=" * 20, "\nDays: ", days)
    print("-" * 20, "\nENG...")
if name == "spring":
    days = 3 * 31
    print("="*20,"\nDays: ",days)
    print("-"*20,"\nENG...")

else:
    print("=" * 20, "\nName is invalid")
    print("-" * 20, "\nENG...")