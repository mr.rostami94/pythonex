"""============================================================
منطق برنامه تعیین کننده اینکه آیا سه عدد a,b,c می توانند طول اضلاع یک مثلث باشند یا نه
============================================================"""

a = float(input("Please enter first number: "))
b = float(input("Please enter second number: "))
c = float(input("Please enter third number: "))

if a + b >= c and b+c>=a and a+c >= b:
    print("=" * 20, "\nYES", "\nGoodbye...")
else:
    print("="*20,"\nNO","\nGoodbye...")
