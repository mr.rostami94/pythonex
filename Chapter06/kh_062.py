"""================================================
منطق برنامه دریافت کننده عدد فرد و مثبت N و
محاسبه کننده مجموع اعداد فرد از 1 تا N و
تعیین کننده تعداد اعداد بخش پذیر بر 3 از این اعداد
================================================"""
n = int(input("Please enter your num: "))


#             with while
#----------------------------------------
if n > 0:
    sigma = 0
    count = 1
    numbers = 0
    while count < n+1:
        if count % 2 != 0:
            sigma += count
            if count % 3 == 0:
                numbers += 1
        count += 1
    print("="*20,"\nSigma = ",sigma,"\nNumbers: ",numbers)
else:
    print("عدد منفی است")



#                with for
#--------------------------------------------
if n > 0:
    sigma = 0
    numbers = 0
    for count in range(1,n+1):
        if count % 2 != 0:
            sigma += count
            if count % 3 == 0:
                numbers += 1
    print("="*20,"\nSigma = ",sigma,"\nNumbers: ",numbers)
else:
    print("عدد منفی است")
