"""==========================================================
با نگرش به اینکه یک عدد D رقمی از ۱۰ به توان D کوچکتر است،
منطق برنامه ای را طراحی کنید که یک عدد طبیعی را در ورودی
دریافت کند سپس تعداد ارقام آن را مشخص و بهمراه عدد چاپ نماید.
=========================================================="""

n = int(input("Please enter your number: "))
D = 1
while n > 10**D:
    D += 1
print("="*20,"\nNumber: ",n,"\nTedad: ",D)
