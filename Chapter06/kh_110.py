"""================================================================
منطق برنامه سازنده مقلوب اعداد چهار رقمی که رقم یکان ان ها صفر نیست
================================================================"""
count = 1000
while count < 10000:
    index = count
    if index % 10 != 0:
        s = 0
        number = index
        while number>0:
            remain = number%10
            s = (s*10)+remain
            number //= 10
        print("="*30,"\nNumber: ",index,"\tMaghloob: ",s)
    count+=1
