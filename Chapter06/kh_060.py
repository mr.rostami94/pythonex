"""====================================================================
منطق برنامه دریافت کننده عدد N که مضرب 11 می باشد و چاپ کننده ضرایب عدد 11 از 11 تا N
و محاسبه مجموع این ضرایب
===================================================================="""

#           with while
#==================================
n = int(input("Please enter your number: "))
sigma = 0
if n % 11 == 0:
    count = 11
    while count < n+1:
        if count % 11 == 0:
            sigma += count
        count += 1
    print("="*20,"\nSigma = ",sigma)
else:
    print("عدد مضرب 11 نیست")


#          with for
#==============================

n = int(input("Please enter your number: "))
sigma = 0
if n % 11 == 0:
    for count in range(11,n+1):
        if count % 11 == 0:
            sigma += count
    print("=" * 20, "\nSigma = ", sigma)
else:
    print("عدد بر 11 بخش پذیر نیست")

