"""======================================================
منطق برنامه محاسبه کننده مجموع اعداد بین 20 تا 2000 که بر 3 و 7 بخش پذیرند
======================================================"""

#with while
count = 20
sigma = 0
while count <=2000:
    if count % 21 == 0:
        sigma += count
    count += 1
print("="*20,"\nSigma = ",sigma)

#with for
sigma = 0
for count in range(20,2001):
    if count % 21 == 0:
        sigma += count
print("="*20,"\nSigma = ",sigma)