"""============================================================
با نگرش به اینکه میانگین هندسی N عدد برابر با ریشه Nام حاصل ضرب
آنهاست، منطق برنامه ای را طراحی کنید که N عدد را در ورودی دریافت
کند و میانگین هندسی انها را محاسبه و چاپ کند.
============================================================"""

numbers = int(input("Please enter numbers of values: "))
temp = 1
count = 1
while count <= numbers:
    print("Please enter ",count," number")
    num = int(input("==>"))
    temp = temp*num
    count+=1
ga = (temp)**(1/numbers)
print("="*20,"\nGA: ",ga)