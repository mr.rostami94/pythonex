"""===============================================================
منطق برنامه دریافت کننده نمرات امتحانی 30 نفر دانشجو و تعیین تعداد نمرات در بازه های مختلف
==============================================================="""

numbers_of_student = 2
count = 1
C = 0
B = 0
A = 0
while count <= numbers_of_student:
    mark = float(input("Please enter number: "))
    if mark < 10:
        C+=1
    elif 10<=mark<=15:
        B+=1
    elif mark > 15:
        A+=1
    count+=1

print("A: ",A,"\tB: ",B,"\tC: ",C)
