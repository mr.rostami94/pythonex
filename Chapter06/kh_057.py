"""========================================
منطق برنامه محاسبه کننده مجموع 12 جمله از یک سری مشخص
========================================"""

# s = 1 - 2/1! + 4/3! -6/5! + ...
#with while
s = 1
count = 1
zarib = 1
while count < 13:
    n = 2*count - 1
    fact = 1
    temp = 1
    while temp <=n:
        fact = fact * temp
        temp += 1
    zarib *= -1
    sentence = zarib * (2*count/fact)
    s += sentence
    count += 1
print("="*20,"\nResult: ",s)

#with for
s = 1
zarib = 1
for count in range(1,13):
    zarib *= -1
    n = 2*count - 1
    fact = 1
    for temp in range(1,n+1):
        fact = fact * temp
    sentence = zarib * (2*count / fact)
    s += sentence
print("="*20,"\nResult: ",s)