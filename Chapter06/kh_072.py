"""=====================================
منطق برنامه مشخص کننده کلیه اعداد کوچکتر از 3000 و
محاسبه کننده مجموع این اعداد
====================================="""
tedad = 0
number = 2
sigma = 0
while number < 3000:
    key = 1
    count = 2
    while count < number/2:
        if number % count == 0:
            key+=1
        count += 1
    if key == 1:
        tedad += 1
        sigma += number
    number += 1

print("="*20,"\nTedad = ",tedad,"\nSigma = ",sigma)


#             with for
#---------------------------------------
tedad = 0
sigma = 0
for number in range(2,3000):
    key = 1
    for count in range(2,number//2):
        if number % count == 0:
            key += 1
    if key == 1:
        tedad += 1
        sigma += number
print("="*20,"\nTedad = ",tedad,"\nSigma = ",sigma)