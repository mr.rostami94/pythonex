"""===================================================================
منطق برنامه دریافت کننده عدد N و تعیین کننده اعداد اول در بازه ۲ تا N
و اعداد تام متناظر با اعداد اول
==================================================================="""

n = int(input("Please enter your number: "))
number = 2
while number < n:
    count = 2
    key = 0
    while count < number:
        if number % count == 0:
            key += 1
        count += 1
    if key == 0:
        total = 2**(count - 1)*(2**count - 1)
        print("="*30,"\nPrime Number: ",count,"\nTotal: ",total)
    number += 1