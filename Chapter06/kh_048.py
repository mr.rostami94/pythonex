"""===================================
منطق برنامه محاسبه کننده فاکتوریل یک عدد صحیح مثبت
==================================="""

#with while
n = int(input("Please enter your num: "))
count = 1
multi = 1
while count <= n:
    multi *= count
    count += 1
print("="*20,"\nResult: ",multi)

#with for
n = int(input("Please enter your num: "))
multi = 1
for count in range(1,n+1):
    multi *= count
print("="*20,"\nResult: ",multi)

