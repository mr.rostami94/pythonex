"""================================================================
منطق برنامه دریافت کننده عدد صحیح N و محاسبه کننده فاکتوریل اعداد
یک تا N
================================================================"""

n = int(input("Please enter your number: "))
number = 1
while number <= n:
    fact = 1
    for temp in range(1, number+1):
        fact = fact*temp
    print("="*30,"\nNumber: ",number,"\tFactorial: ",fact)
    number += 1
