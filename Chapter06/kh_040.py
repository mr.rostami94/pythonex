"""============================
منطق برنامه محاسبه کننده اعداد زوج دو رقمی
============================"""

#with while

count = 10
sigma = 0
while count < 100:
    if count % 2 == 0:
        sigma += count
    count += 2

print("="*20,"\nSigma = ",sigma)

#with for
sigma = 0
for count in range(10,100,2):
    if count % 2 == 0:
        sigma += count

print("="*20,"\nSigma = ",sigma)