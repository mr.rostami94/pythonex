"""=====================================================
منطق برنامه ای را طراحی کنید که عدد صحیح و مثبت N را در ورودی دریافت کند
سپس مجموع N-1 جمله از سری زیر را محاسبه و چاپ کند.

s = 1/2 + 2/3 + 3/4 + ...
===================================================="""

n = int(input("Please enter your number: "))

#         with while
#-----------------------------------
count = 1
sigma = 0
while count < n:
    obj = count/(count + 1)
    sigma += obj
    count += 1

print("="*20,"\nSigma = ",sigma)



#         with for
#-------------------------------
sigma = 0
for count in range(1,n):
    obj = count/(count+1)
    sigma += obj
print("="*20,"\nSigma = ",sigma)
