"""========================================================
منطق برنامه جابجا کننده ارقام یکان و دهگان اعداد دو رقمی
========================================================"""

n = int(input("Please enter N: "))
while True:
    if n >= 100:
        n = int(input("Please enter N: "))
        continue
    elif n >= 10 and n < 100:
        s = 0
        while n > 0:
            r = n % 10
            s = (s * 10) + r
            n //= 10
        print("=" * 20, "\nResult: ", s)
        n = int(input("Please enter N: "))
        continue
    else:
        print("عدد یک رقمی است و تمام...")
        break