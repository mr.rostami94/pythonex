"""==============================================================
منطق برنامه تعیین کننده اعداد صحیح کوچکتر از هزار که بر 3 یا 7 یا 11 بخش پذیر هستند
=============================================================="""
#with while
numbers = 0
count = 1
while count < 1000:
    if count % 3 == 0 or count % 7 == 0 or count % 11 == 0:
        numbers += 1
    count += 1
print("="*20,"\nNumbers: ",numbers)



#with for
numbers = 0
for count in range(1,1000):
    if count % 3 == 0 or count % 7 == 0 or count % 11 == 0:
        numbers += 1
print("="*20,"\nNumbers: ",numbers)