"""======================================
منطق برنامه محاسبه کننده مجموع مضارب دو رقمی عدد چهار
======================================"""

#with while
count = 10
sigma = 0
while count < 100:
    if count % 4 == 0:
        sigma += count
    count += 1

print("="*20,"\nSigma = ",sigma)

#with for
sigma = 0
for count in range(10,100):
    if count % 4 == 0:
        sigma += count
print("="*20,"\nSigma = ",sigma)