"""==========================================
منطق برنامه تعیین کننده نوع کارمند براساس جنسیت و کد تحصیلات
گروه 1: کارمندان زن با تحصیلات کمتر از دیپلم
گروه 2: کارمندان زن با تحصیلات دیپلم یا فوق دیپلم
گروه 3: کارمندان زن با تحصلات لیسانس و بالاتر
گروه 4: کارمندان مرد با تحصیلات کمتر از دیپلم
گروه 5: کارمندان مرد با تحصلات دیپلم یا فوق دیپلم
گروه 6: کارمندان مرد با تحصیلات لیسانس و بالاتر
=========================================="""
#sex : 1 ==> Man , 2 ==> Woman
#Madrak: 11 ==> kamtar az diplom
#Madrak: 12 ==> diplom ya fogh-diplom
#Madrak: 13 ==> lisans ya balatar

while True:
    id = int(input("ID: "))
    if id == 0:
        break
    sex = int(input("Sex: "))
    madrak = int(input("Madrak: "))
    if sex == 2 and madrak == 11:
        print("ID: ",id,"\tGroup: Group1")
    if sex == 2 and madrak == 12:
        print("ID: ",id,"\tGroup: Group2")
    if sex == 2 and madrak == 13:
        print("ID: ",id,"\tGroup: Group3")
    if sex == 1 and madrak == 11:
        print("ID: ",id,"\tGroup: Group4")
    if sex == 1 and madrak == 12:
        print("ID: ",id,"\tGroup: Group5")
    if sex == 1 and madrak == 13:
        print("ID: ",id,"\tGroup: Group6")


print("END PROGRAM...")
