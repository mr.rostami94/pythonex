"""===================================
منطق برنامه تعیین بزرگترین عدد ما بین 80 عدد ورودی
==================================="""

#            with while
#--------------------------------------

num = int(input("Please enter your num: "))
max = num
count = 1
while count < 80:
    num = int(input("Please enter your num: "))
    if num > max:
        max = num
    count += 1
print("="*20,"\nMax = ",max)



#         with for
#------------------------------------

num = int(input("Please enter num: "))
max = num
for count in range(1,80):
    num = int(input("Please enter num: "))
    if num > max:
        max = num
print("="*20,"\nMax = ",max)