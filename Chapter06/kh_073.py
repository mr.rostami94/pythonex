"""================================================
منطق برنامه تولید کننده 21 جمله از یک سری و محاسبه کننده میانگین آنها

s = 1 - 1/3 + 1/5 - 1/7 + ...
================================================"""



#               with while
#----------------------------------------
count = 0
start = 0
sigma = 0
zarib = -1
while count < 21:
    zarib *= -1
    obj = zarib * (1/(2*start + 1))
    sigma += obj
    start += 1
    count += 1

print("="*20,"\nResult = ",sigma)



#             with for
#-----------------------------------
start = 0
zarib = -1
sigma = 0
for count in range(21):
    zarib *= -1
    obj = zarib * 1/(2*start + 1)
    sigma += obj
    start += 1
print("="*20,"\nResult = ",sigma)