"""=======================================
منطق برنامه محاسبه کننده مجموع n جمله از یک سری مشخص

s = 1/3 + 2/3^2 + 3/3^3 + ...
======================================="""

n = int(input("Please enter your number: "))


#        with while
#--------------------------------
count = 1
sigma = 0
while count < n+1:
    obj = count/(3**count)
    sigma += obj
    count+=1
print("="*20,"\nSigma = ",sigma)


#        with for
#-----------------------------
sigma = 0
for count in range(n+1):
    obj = count/(3**count)
    sigma += obj
print("="*20,"\nSigma = ",sigma)


