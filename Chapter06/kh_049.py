"""=============================================
منطق برنامه محاسبه کننده درآمد ماهیانه یک ویزیتور از فروش کالایی
============================================="""

number_of_day = int(input("Please enter number of days: "))
total = 0
start = 60
count = 1
while count <= number_of_day:
    total += start
    start += 10
    count += 1

income = (total*400*10)/100
print("="*20,"\nIncome: ",income)