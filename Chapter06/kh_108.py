"""================================================================
منطق برنامه محاسبه کننده میانگین نمرات ۳۵ نفر دانش اموز در درس شیمی
و تعیین کننده بالاترین و پایین ترین نمره
================================================================"""
count = 1
print("Please enter number ",count,"th student")
mark = float(input("===>"))
sigma = mark
min = mark
max = mark
while count < 3:
    count += 1
    print("Please enter number ",count,"th student")
    mark = float(input("===>"))
    sigma += mark
    if mark < min:
        min = mark
    if mark > max:
        max = mark
average = sigma / 3
print("="*50,"\nAverage: ",average,"\tMax: ",max,"\tMin: ",min)