"""===================================
منطق برنامه مشخص کننده مقسوم علیه های عدد N و
تعیین کننده تعداد و
مجموع انها
==================================="""
n = int(input("Please enter your num: "))

#           with while
#-----------------------------------
count = 1
numbers = 0
sigma = 0
while count <= n:
    if n % count == 0:
        numbers += 1
        sigma += count
    count += 1
print("="*20,"\nNumbers = ",numbers,"\nSigma = ",sigma)


#          with for
#------------------------------------
numbers = 0
sigma = 0
for count in range(1,n+1):
    if n % count == 0:
        numbers += 1
        sigma += count
print("="*20,"\nNumbers = ",numbers,"\nSigma = ",sigma)