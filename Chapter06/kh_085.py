"""===========================================================
منطق برنامه محاسبه کننده تعداد ترکیبات ممکن k شی از میان n شی
==========================================================="""

k = int(input("Please enter K(less): "))
n = int(input("Please enter N(bigger): "))

#result = n!/(k! * (n-k)!)

#        N!
#--------------------------
fn = 1
for temp in range(1,n+1):
    fn *= temp


#    K!
#-----------------------
fk = 1
for temp in range(1,k+1):
    fk *= temp

#     (N-K)!
#--------------------------
nk = n - k
fnk = 1
for temp in range(1,nk+1):
    fnk *= temp

#       Result
#--------------------
result = fn/(fk*fnk)

print("="*20,"\nResult = ",result)