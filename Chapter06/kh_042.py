"""=========================================
منطق برنامه محاسبه کننده مجموع 99 جمله از یک سری مشخص
========================================="""

#with while

count = 2
sigma = 0
while count <= 100:
    n = 1/(count**2)
    sigma += n
    count += 1
print("="*20,"\nSigma = ",sigma)

#with for
sigma = 0
for count in range(2,101):
    n = 1/(count**2)
    sigma += n
print("="*20,"\nSigma = ",sigma)