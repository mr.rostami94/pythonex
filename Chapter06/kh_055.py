"""=========================================
منطق برنامه محاسبه کننده مجموع 20 جمله اول یک سری مشخص
========================================="""

#S = -1 + 1/2 -1/4 + 1/6 - ...
#with while
sigma = -1
count = 2
zarb = -1
while count < 20:
    zarb *= -1
    sentence = zarb*(1/count)
    sigma += sentence
    count += 2
print("="*20,"\nResult: ",sigma)

#with for
sigma = -1
zarb = -1
for count in range(2,20,2):
    zarb *= -1
    object = zarb*(1/count)
    sigma += object
print("="*20,"\nResult: ",sigma)