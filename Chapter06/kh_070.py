"""=====================================================
منطق برنامه تعیین کننده تعداد ارقام عدد طبیعی N و محاسبه کننده مجموع ارقام آن
====================================================="""
n = int(input("Please enter your number: "))
numbers = 0
sigma = 0
while n > 0:
    r = n%10
    numbers += 1
    sigma += r
    n = n //10
print("="*20,"\nNumbers: ",numbers,"\nSigma = ",sigma)