"""=======================================================
منطق برنامه محاسبه کننده مجموع اعداد مضرب 3 و 6 و 4 بزرگتر از 15 و کوچکتر
از 4000
======================================================="""

#with while
count = 15
sigma = 0
while count <= 4000:
    if count % 12 == 0:
        sigma += count
    count += 1
print("="*20,"\nSigma = ",sigma)

#with for
sigma = 0
for count in range(15,4001):
    if count % 12 == 0:
        sigma += count
print("="*20,"\nSigma = ",sigma)