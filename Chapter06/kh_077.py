"""====================================================
منطق برنامه ای را طراحی نمایید که عدد طبیعی فرد N و عدد
حقیقی X را در ورودی دریافت کند سپس جملات سری زیر را
محاسبه و چاپ کند.

s = x + x^3/3 + x^5/5 + ...
====================================================="""

n = int(input("Please enter N: "))
if n % 2 != 0:
    x = float(input("Please enter X: "))
    count = 1
    sigma = 0
    while count <= n:
        sentence = (x**count)/count
        sigma += sentence
        count += 2
    print("="*20,"\nSigma = ",sigma)
else:
    print("این عدد زوج است.")