"""==============================================================
منطق برنامه محاسبه کننده معدل هر یک از ۳۰ نفر دانشجویان کلاسی در
سه درس و تعیین معدل کل دانشجویان
=============================================================="""

count = 1
while count <= 30:
    print("For ",count,"th Student","\n","-"*30)
    sigma = 0
    sigma_value = 0
    num = 1
    while num <= 3:
        print("Please enter ",num," mark")
        mark = float(input("===>"))
        vahed = int(input("Please enter value for this lesson: "))
        sigma += mark*vahed
        sigma_value += vahed
        num += 1
    average = sigma/sigma_value
    print("="*30,"\nAverage for this student = ",average)
    print("#"*40)
    count += 1