"""=========================================================
منطق برنامه مشخص کننده اعداد تام کوچکتر از یک عدد طبیعی معین
========================================================="""

n = int(input("Please enter N: "))
k = 1
numbers = 0
while k < n:
    count = 1
    sigma = 0
    while count < k:
        if k % count == 0:
            sigma += count
        count += 1
    if sigma == k:
        print(k, end="  ")
        numbers += 1
    k += 1
print()
print("="*20,"\nNumbers: ",numbers)