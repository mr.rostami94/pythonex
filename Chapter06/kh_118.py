"""=================================================================
منطق برنامه تعیین کننده اینکه چهار عدد ورودی تشکیل یک تصاعد هندسی
را می دهند یا خیر

و در صورت تشکیل دادن تصاعد مقدار N را دریافت و N عدد بعدی را مشخص
و چاپ کند.
================================================================="""

num1 = int(input("Please enter Num1: "))
num2 = int(input("Please enter Num2: "))
num3 = int(input("Please enter Num3: "))
num4 = int(input("Please enter Num4: "))

if num2 / num1 == num3 / num2 == num4 / num3:
    print("="*20,"\nاین یک تصاعد هندسی است","\n","-"*20)
    n = int(input("Please enter N: "))
    r = num2 / num1
    num = num4
    for i in range(n):
        num = num * r
        print(num,end=" ")
else:
    print("="*20,"\nاین یک تصاعد هندسی نیست")