"""=============================================================
منطق برنامه تولید کننده 25 عدد از یک سری مشخص و محاسبه کننده مجموع و مانگین این اعداد
============================================================="""

#s = 1,4,7,10,...

#with while
#================================
start = 1
sigma = 0
count = 0
while count < 25:
    sigma += start
    start += 3
    count += 1
average = sigma / 25
print("="*20,"\nSigma: ",sigma)
print("="*20,"\nAverage: ",average)



#with for
#==================================

start = 1
sigma = 0
for count in range(25):
    sigma += start
    start += 3
average = sigma / 25
print("="*20,"\nSigma: ",sigma)
print("="*20,"\nAverage: ",average)