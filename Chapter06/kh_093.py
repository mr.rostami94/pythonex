"""============================================================
منطق برنامه تعیین کننده تعداد ارقام زوج، فرد و صفر یک عدد طبیعی
============================================================"""

number = int(input("Please enter your number: "))
odd = 0
even= 0
zero = 0
while number > 0:
    remain = number % 10
    if remain == 0:
        zero += 1
    elif remain % 2 == 0:
        even += 1
    else:
        odd += 1
    number = number//10
print("="*20,"\nZero: ",zero,"\nOdd: ",odd,"\nEven: ",even)