"""================================================================
منطق برنامه دریافت کننده ۵۰ عدد و مشخص کننده اعداد مثبت، منفی و صفر
در بین آنها
================================================================"""
positive = 0
negative = 0
zero = 0
count = 0
while count < 50:
    number = int(input("Please enter your number: "))
    if number > 0:
        positive += 1
    elif number < 0:
        negative += 1
    else:
        zero += 1
    count += 1

print("="*50,"\nZero = ",zero,"\nPositive = ",positive,"\nNagative =",negative)
