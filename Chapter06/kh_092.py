"""===================================================
منطق برنامه تبدیل کننده دو رقم سمت چپ تلفن از ۴۳ به ۳۴
==================================================="""

telephone = 0
while telephone != 9999999:
    telephone = int(input("Please enter phone number: "))
    if telephone < 4300000:
        continue
    elif telephone >= 4400000:
        continue
    telephone = telephone - 900000
    print("Telephone: ",telephone)
print("End Program")