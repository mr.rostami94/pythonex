"""==========================
منطق برنامه سازنده مقلوب یک عدد صحیح
=========================="""

n = int(input("Please enter your number: "))
s = 0
while n > 0:
    r = n % 10
    s = (s * 10) + r
    n = n//10
print("="*20,"\nResult = ",s)