"""================================
منطق برنامه محاسبه کننده میانگین 90 عدد ورودی
================================="""

#            with while
#----------------------------------
sigma = 0
count = 0
while count < 90:
    num = int(input("Please enter your num: "))
    sigma += num
    count += 1

average = sigma / 90
print("="*20,"\nSigma = ",sigma,"\nAverage = ",average)


#              with for
#--------------------------------------
sigma = 0
for count in range(90):
    num = int(input("Please enter your num: "))
    sigma += num
average = sigma / 90
print("="*20,"\nSigma = ",sigma,"\nAverage = ",average)