"""==============================================================
منطق برنامه محاسبه کننده مجموع اعداد مضرب 2 و 7 و 3 بزرگتر از 32 و کوچکتر از 5000
=============================================================="""

#with while
count = 32
sigma = 0
while count <=5000:
    if count % 42 == 0:
        sigma += count
    count += 1
print("="*20,"\nResult = ",sigma)

#with for
sigma = 0
for count in range(32, 5001):
    if count % 42 == 0:
        sigma += count
print("="*20,"\nResult = ",sigma)