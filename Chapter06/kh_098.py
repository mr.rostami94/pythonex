"""==============================================================
منطق برنامه محاسبه کننده مجذور عدد طبیعی N بر این اساس که مجموع N
عدد فرد متوالی با مجذور N برابر است.
=============================================================="""
number = int(input("Please enter your number: "))
p = 2*(number) - 1
count = 0
square = 0
while count <= p:
    if count % 2 != 0:
        square += count
    count += 1

print("="*20,"\nSquare for",number,"=",square)