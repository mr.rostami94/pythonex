"""=============================================================
منطق برنامه مشخص کننده اعداد مضرب M از عدد صحیح L تا عدد صحیح P
و محاسبه کننده تعداد اینگونه اعداد.
============================================================="""

start = int(input("Please enter L: "))
end = int(input("Please enter P: "))
m = int(input("Please enter M: "))
numbers = 0
if start > end:
    start,end = end,start
while start < end:
    if start % m == 0:
        print(start)
        numbers += 1
    start += 1
print("="*20,"\nNumbers: ",numbers)
