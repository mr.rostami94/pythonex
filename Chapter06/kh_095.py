"""=============================================================
منطق برنامه مشخص کننده اینکه عدد طبیعی N بر مجموع ارقامش بخش پذیر
است یا نه.
============================================================="""

num = int(input("Please enter your number: "))
number = num
sigma = 0
while number > 0:
    remain = number % 10
    sigma += remain
    number //=10
if num % sigma == 0:
    print("="*20,"\nYES")
else:
    print("=" * 20, "\nNO")