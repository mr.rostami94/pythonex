"""===================================
منطق برنامه تعیین کوچکترین عدد ما بین 60 عدد ورودی
==================================="""

#            with while
#--------------------------------------

num = int(input("Please enter your num: "))
min = num
count = 1
while count < 60:
    num = int(input("Please enter your num: "))
    if num < min:
        min = num
    count += 1
print("="*20,"\nMin = ",min)



#         with for
#------------------------------------

num = int(input("Please enter num: "))
min = num
for count in range(1,60):
    num = int(input("Please enter num: "))
    if num < min:
        min = num
print("="*20,"\nMin = ",min)