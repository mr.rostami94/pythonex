"""====================================
منطق برنامه تعیین کننده مجموع اعداد سه رقمی مضرب 5
===================================="""

#with while
count = 100
sigma = 0
while count < 1000:
    if count  % 5 == 0:
        sigma += count
    count +=1
print("="*20,"\n Sigma= ",sigma)

#with for
sigma = 0
for count in range(100,1000):
    if count % 5 == 0:
        sigma += count
print("="*20,"\n Sigma= ",sigma)