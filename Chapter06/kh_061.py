"""=========================================================================
منطق برنامه دریافت کننده عدد زوج و مثبت N و محاسبه کننده مجموع اعداد فرد و زوج از صفر تا N به تفکیک
========================================================================="""

n = int(input("Please enter your num: "))

#                with while
#----------------------------------------
if n > 0:
    sigma_zoj = 0
    sigma_fard = 0
    count = 0
    while count < n+1:
        if count % 2 == 0:
            sigma_zoj += count
        else:
            sigma_fard += count
        count += 1
    print("="*20,"\nSimga Zoj: ",sigma_zoj,"\nSimga Fard: ",sigma_fard)
else:
    print("عدد منفی است")



#           with for
#------------------------------------------
if n > 0:
    sigma_fard = 0
    sigma_zoj = 0
    for count in range(n+1):
        if count % 2 == 0:
            sigma_zoj += count
        else:
            sigma_fard += count
    print("=" * 20, "\nSimga Zoj: ", sigma_zoj, "\nSimga Fard: ", sigma_fard)
else:
    print("منفی")
