"""================================================================
منطق برنامه زیربرنامه تعیین کننده بزرگترین مقسوم علیه مشترک دو عدد
به صورت خودفراخوان
================================================================"""

def bmm(m,n):
    if n==0:
        return m
    else:
        r = m%n
        return bmm(n,r)

number1 = int(input("Please enter first number: "))
number2 = int(input("Please enter second number: "))
if number1 < number2:
    number1,number2=number2,number1
print("BMM: ",bmm(number1,number2))