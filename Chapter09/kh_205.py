"""================================================================
منطق برنامه ای را طراحی کنید که تعداد عناصر یک ارایه اعشاری را
بخواند و به دنبال آن عناصر آرایه را در ورودی دریافت دارد و در
آرایه ذخیره کنید. سپس با فراخوانی یک زیرروال اعداد را به صورت صعودی
مرتب کند و سرانجام آرایه مرتب شده را چاپ کند.
================================================================"""

#--------------الگوریتم حبابی----------------


def bubble_sort(mylist,sort=1):
    if sort==-1:
        for i in range(len(mylist)):
            for j in range(len(mylist)-(i+1)):
                if mylist[j] < mylist[j+1]:
                    temp = mylist[j]
                    mylist[j] = mylist[j+1]
                    mylist[j+1] = temp
        return mylist
    elif sort==1:
        for i in range(len(mylist)):
            for j in range(len(mylist)-(i+1)):
                if mylist[j] > mylist[j+1]:
                    temp = mylist[j]
                    mylist[j] = mylist[j+1]
                    mylist[j+1] = temp
        return mylist
    else:
        return "ارگومان ورودی صحیح نیست"


n = int(input("Please enter numbers of list: "))
mylist = []
for i in range(1,n+1):
    print("Please enter number",i)
    num = float(input("==> "))
    mylist.append(num)

mylist = bubble_sort(mylist)
print(mylist)
