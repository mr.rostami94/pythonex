"""================================================================
منطق برنامه محاسبه کننده فاکتوریل یک عدد به صورت خود فراخوان
================================================================"""

def fact(n):
    if n == 1:
        return 1
    else:
        return n*fact(n-1)

n = int(input("Please enter your num: "))
print("FACT: ",fact(n))