"""================================================================
منطق برنامه ای را طراحی کنید که تعداد عناصر یک آرایه حاوی اعداد صحیح
را بخواند و به دنبال آن عضوهای آرایه را در ورودی دریافت کند و در آرایه
ذخیره کند سپس با فراخوانی یک زیرروال کوچکترین و بزرگترین عنصر آرایه را
تعیین کند و نتیجه را چاپ کند.
================================================================"""

def max_min(A):
    max = A[0]
    min = A[0]
    for i in A:
        if i>max:
            max = i
        elif i < min:
            min = i
    return max,min

n = int(input("Please enter numbers of list: "))
mylist = []
for i in range(1,n+1):
    print("Please enter number",i)
    num = int(input("==> "))
    mylist.append(num)


print("MAX: ",max_min(mylist)[0],"\tMIN: ",max_min(mylist)[1])