"""================================================================
منطق برنامه ای را طراحی کنید که تعداد سطرها و ستون های دو جدول مشابه
را بخواند سپس با فراخوانی یک زیر روال عناصر آنها را بخواند و در
آرایه های B,A ذخیره کندو با فراخوانی یک زیر روال دیگر عناصر متناظر
دو جدول را با هم جمع کند و سپس با فراخوانی زیر روال دیگری آرایه
حاصل جمع را به نام D چاپ کند.
================================================================"""



#-----------Create Function--------------------
def create_mat(n,m):
    A = []
    for i in range(n):
        row = []
        for j in range(m):
            print("Please enter number in row",i+1,"and column",j+1)
            num = int(input("==> "))
            row.append(num)
        A.append(row)
    return A


#--------------Sum Function------------------
def sum_mat(A,B):
    C = []
    for i in range(len(A)):
        row = []
        for j in range(len(A[0])):
            row.append(A[i][j]+B[i][j])
        C.append(row)
    return C

C = [[4, 6, 8], [2, 4, 6]]
def show_mat(C):
    for i in range(len(C)):
        for j in range(len(C[0])):
            print(C[i][j],end="\t")
        print()
    return


n = int(input("Please enter row: "))
m = int(input("Please enter column: "))
print("-"*10,"Create Matrix A","-"*10)
A = create_mat(n,m)
print("="*20)
show_mat(A)


print("-"*10,"Create Matrix B","-"*10)
B = create_mat(n,m)
print("="*20)
show_mat(B)


print("-"*10,"Show RESULT","-"*10)
D = sum_mat(A,B)
show_mat(D)



