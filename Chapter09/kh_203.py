"""================================================================
منطق برنامه ای را طراحی کنید که ده جمله که هر کدام حداکثر ۷۰ نویسه
دارد را بخواند و سپس با فرض اینکه کلمات با یک فاصله از هم جدا شده
و انتهای هر جمله به نماد . ختم شده است با فراخوانی یک زیرروال تعداد
کلمات هر جمله را شمارش نماید و چاپ کند.
================================================================"""
A = "Mohammad Rostami Ali Mohsen"
def count_word(A):
    count = 1
    for i in range(len(A)):
        if A[i] == ' ':
            count += 1
    return count

for i in range(1,3):
    print("Please enter sentence",i)
    sent = input("==> ")
    count = count_word(sent)
    print("This sentence has",count,"word.")
