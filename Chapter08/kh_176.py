"""============================================================
منطق برنامه ای را طراحی نمایید که ۵۰ عدد را بخواند و در آرایه A
ذخیره کند سپس آنها را به صورت صعودی مرتب و چاپ کند.
============================================================"""

#           Make Matrix A
#------------------------------------------
A = []
for i in range(1,6):
    print("Please enter number",i)
    num = int(input("==> "))
    A.append(num)

print(A)
print("="*30)

# #            مرتب سازی انتخابی
# #-------------------------------------------
# #    MAX A
# #-------------
max = A[0]
for temp in A:
    if temp > max:
        max = temp


#     Create List B
#------------------------------
B = []
for i in range(len(A)):
    min = A[0]
    index = 0
    for j in range(len(A)):
        if A[j] < min:
            min = A[j]
            index = j
    A[index] = max + 1
    B.append(min)

print("="*30)
print(B)



#          مرتب سازی جابجایی
#----------------------------------------
for i in range(len(A)):
    for j in range(len(A)-(i+1)):
        if A[j] > A[j+1]:
            temp = A[j]
            A[j] = A[j+1]
            A[j+1] = temp
    print(A)