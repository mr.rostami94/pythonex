"""============================================================
با فرض اینکه نام و کد ملی ۲۰۰ نفر شهروند قبلاً در دو آرایه متناظر
و مکمل هم به نام های NCODE,NAME ذخیره شده است و ارایه NCODE به
صورت صعودی مرتب شده است منطق برنامه ای را طراحی کنید که یک کد ملی
را بخواند و آنرا در NCODE جستجو کند و اگر یافت شود نام شهروند
دارای چنین کدی را چاپ کند در غیر اینصورت پیام مناسبی را چاپ کند
============================================================"""
import sort_algoritm
NAME = ["Mohammad","Ali","Mohsen"]
NCODE = [2,3,4]
req = int(input("Please enter ncode: "))
temp = False
mylist = sort_algoritm.bubble_sort(NCODE)
while len(mylist) > 0:
    key = len(mylist)//2
    if mylist[key] == req:
        temp = True
        print("NCODE: ",mylist[key],"==>\t NAME: ",NAME[key])
        break
    elif mylist[key] < req:
        mylist = mylist[key+1:]
        NAME = NAME[key+1:]
    elif mylist[key] > req:
        mylist = mylist[:key]
        NAME = NAME[:key]
if temp == False:
    print("داده شما موجود نیست")

