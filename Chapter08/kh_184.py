"""============================================================
با فرض اینکه نام و شماره عضویت ۴۰۰ نفر سهامداران یک شرکت سهامی
عام قبلاً در دو ارایه متناظر و مکمل هم به نام های NAME,MEMB ذخیره
شده اند منطق برنامه ای را طراحی کنید که یک شماره را بخواند و آنرا
در آرایه MEMB جستجو کند و اگر یافت شود نام سهامدار دارنده این شماره
را چاپ کند در غیر اینصورت پیام مناسبی را چاپ کند
============================================================"""

NAME = ["Mohammad","Ali","Mohsen"]
MEMB = [2,3,4]
req = int(input("Please enter ncode: "))
temp = False
while len(MEMB) > 0:
    key = len(MEMB)//2
    if MEMB[key] == req:
        temp = True
        print("NCODE: ",MEMB[key],"==>\t NAME: ",NAME[key])
        break
    elif MEMB[key] < req:
        MEMB = MEMB[key+1:]
        NAME = NAME[key+1:]
    elif MEMB[key] > req:
        MEMB = MEMB[:key]
        NAME = NAME[:key]
if temp == False:
    print("داده شما موجود نیست")

