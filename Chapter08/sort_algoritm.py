"""
تمامی الگوریتم های مرتب سازی
"""

#---------------------------------------------
def max_arr(arr):
    """
    ماکزیمم یک آرایه را محاسبه می کند
    :param arr: ورودی یک ارایه هست
    :return: ماکزیمم یک آرایه را برمیگرداند
    """
    max = arr[0]
    for i in arr:
        if i > max:
            max = i
    return max

#-----------------------------------------------

def min_arr(arr):
    """
    مینیمم یک آرایه را محاسبه می کند
    :param arr: ورودی یک ارایه هست
    :return: مینیمم یک آرایه را برمیگرداند
    """
    min = arr[0]
    for i in arr:
        if i < min:
            min = i
    return min


#------------انتخابی-------------

def selection_sort(mylist):
    sort_list = []
    for i in range(len(mylist)):
        min = mylist[i]
        key = i
        for j in range(len(mylist)):
            if mylist[j] < min:
                min = mylist[j]
                key = j
        sort_list.append(min)
        mylist[key] = max_arr(mylist) + 1
    return sort_list

#---------------جابجایی----------------

def relocation_sort(mylist):
    """
    یک لیست ورودی را با الگوریتم جابجایی مرتب می کند
    :param mylist: لیست ورودی
    :return: لیست مرتب شده با الگوریتم جابجایی
    """
    for i in range(len(mylist)):
        min = mylist[i]
        index = i
        for j in range(i+1,len(mylist)):
            if mylist[j] < min:
                min = mylist[j]
                index = j
        temp = min
        mylist[index] = mylist[i]
        mylist[i] = temp
    return mylist


#--------------الگوریتم حبابی----------------


def bubble_sort(mylist,sort=1):
    if sort==-1:
        for i in range(len(mylist)):
            for j in range(len(mylist)-(i+1)):
                if mylist[j] < mylist[j+1]:
                    temp = mylist[j]
                    mylist[j] = mylist[j+1]
                    mylist[j+1] = temp
        return mylist
    elif sort==1:
        for i in range(len(mylist)):
            for j in range(len(mylist)-(i+1)):
                if mylist[j] > mylist[j+1]:
                    temp = mylist[j]
                    mylist[j] = mylist[j+1]
                    mylist[j+1] = temp
        return mylist
    else:
        return "ارگومان ورودی صحیح نیست"



#--------------مرتب سازی درجی-----------------

def insert_sort(mylist,sort=1):
    if sort==1:
        for i in range(len(mylist)):
            temp = mylist[i]
            index = i
            for j in range(i-1,-1,-1):
                if mylist[j] > temp:
                    mylist[j+1] = mylist[j]
                    index = j
            mylist[index]=temp
        return mylist
    elif sort==-1:
        for i in range(len(mylist)):
            temp = mylist[i]
            index = i
            for j in range(i-1,-1,-1):
                if mylist[j] < temp:
                    mylist[j+1] = mylist[j]
                    index = j
            mylist[index]=temp
        return mylist
    else:
        return "آرگومان ورودی صحیح نیست"


#-------------الگوریتم مرتب سازی شاخصی با درجی ---------------
mylist = [5,13,6,16,19]

