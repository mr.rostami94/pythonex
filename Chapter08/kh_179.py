"""===================================================================
با فرض اینکه نمرات ۲۵ نفر دانشجویان کلاسی قبلاً دریافت شده و پس از
معدل گیری معدل انان در ارایه B ذخیره شده است، منطق برنامه ای را
طراحی نمایید که معدل دانشجویان را با بهره گیری از یک نشانه عملیاتی
به صورت نزولی مرتب و چاپ کند.
==================================================================="""

from sort_algoritm import selection_sort
from sort_algoritm import relocation_sort
from sort_algoritm import bubble_sort
from sort_algoritm import insert_sort
import random



mylist1 = []
for i in range(25):
    mylist1.append(random.randint(1,20))

mylist2 = []
for i in range(25):
    mylist2.append(random.randint(1,20))

mylist3 = []
for i in range(25):
    mylist3.append(random.randint(1,20))


mylist4 = []
for i in range(25):
    mylist4.append(random.randint(1,20))


sort_list_selection = selection_sort(mylist1)
sort_list_relocation = relocation_sort(mylist2)
sort_list_bubble = bubble_sort(mylist3)
sort_list_insert = insert_sort(mylist4)

print("Selection Sort:",sort_list_selection)
print("Relocation Sort:",sort_list_relocation)
print("Bubble Sort:",sort_list_bubble)
print("Insert Sort:",sort_list_insert)