def myfind(mylist,req):
    temp = False
    while len(mylist) > 0:
        key = len(mylist)//2
        if mylist[key] == req:
            temp = True
            break
        elif mylist[key] < req:
            mylist = mylist[key+1:]
        elif mylist[key] > req:
            mylist = mylist[:key]
    return temp
