"""==================================================================
با فرض اینکه ۱۰۰ عدد قبلاً در آرایه A ذخیره شده است منطق برنامه ای را
طراحی کنید که آرایه A را به صورت صعودی مرتب کند.
=================================================================="""
from random import randint
mylist = []
for temp in range(20):
    num = randint(1,300)
    mylist.append(num)


#               مرتب سازی انتخابی
#----------------------------------------------
#   MAX mylist
#-------------------
max = mylist[0]
for i in mylist:
    if i > max:
        max = i

#       Create Sort List
#----------------------------------
sort_list = []
for i in range(len(mylist)):
    min = mylist[0]
    index = 0
    for j in range(len(mylist)):
        if mylist[j] < min:
            min = mylist[j]
            index = j
    sort_list.append(min)
    mylist[index] = max + 1

print("="*30,"\n",sort_list)