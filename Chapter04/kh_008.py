"""=====================================
منطق برنامه تبدیل کننده اندازه یک زاویه از رادیان به درجه
====================================="""

# Degree = (Radian * 360)/2*pi
from math import pi
radian = float(eval(input("مضرب پی را به صورت صحیح یا کسری وارد کنید ==>")))
radian = pi * radian
degree = (radian * 360)/(2*pi)
print(degree)