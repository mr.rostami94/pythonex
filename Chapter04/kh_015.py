"""===================================
منطق برنامه تبدیل کننده طول قد از سانتیمتر به اینچ و وزن
از کیلوگرم به پوند
===================================="""

#1kg = 2.208 pound
#1 yard = 36 inch = 91.44 cm

w = float(input("Please enter weight in Kg: "))
cm = float(input("Please enter height in cm: "))
weight = w * 2.208
inch = (36*cm) / 91.44

print("Weight: ",weight,"Pound","\nInch:",inch,"inch")

