"""========================================
منطق برنامه محاسبه کننده تعداد ارقام یک عدد با بهره گیری از جزء
صحیح لگاریتم آن عدد
========================================"""
from math import log
num = int(input("Please enter your number: "))
if num < 0:
    num = -num
length = int(log(num,10)+1)
print("Length: ",length)