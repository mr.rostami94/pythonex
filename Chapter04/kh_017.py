"""========================================
منطق برنامه تبدیل کننده تعداد روزهای ماه به ساعت و دقیقه و ثانیه
========================================"""

days = int(input("Please enter numbers of days: "))
hour = days * 24
minute = hour * 60
seconds = minute * 60

print("Hour: ",hour,"\nMinute: ",minute,"\nSeconds: ",seconds)