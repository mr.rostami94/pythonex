"""==========================================
منطق برنامه تعیین کننده اختلاف مساحت دایره و مربع محصور کننده آن
=========================================="""

from math import pi
r = float(input("Please enter radius: "))

d = 2*r
s_square = d**2
s_circle = pi*(r**2)

different = s_square - s_circle

print("Different: ",different)