"""===================================================
منطق برنامه دریافت کننده ۶ عدد و محاسبه کننده حاصل جمع ضرب عدد اول در ششم
و ضرب عدد دوم در پنجم و مربعات اعداد سوم و چهارم
==================================================="""
a1 = float(input("Please enter num1: "))
a2 = float(input("Please enter num2: "))
a3 = float(input("Please enter num3: "))
a4 = float(input("Please enter num4: "))
a5 = float(input("Please enter num5: "))
a6 = float(input("Please enter num6: "))

sigma = (a1*a6) + (a2*a5) + a3**2 + a4**2
print("Sigma: ",sigma)