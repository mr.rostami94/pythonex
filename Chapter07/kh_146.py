"""======================================================================
منطق برنامه ای را طراحی کنید که به ترتیب شماره کار اختصاص داده شده به هر
یک از ۲۰ نفر کارکنان یک کارگاه ساختمانی که در کارهای مختلف با دستمزد متفاوت
کار می کنند و تعداد ساعات کار در هفته و دستمزد هر کدام را در ورودی دریافت
کند و در آرایه ای ذخیره کند.
سپس دستمزد هفتگی هر یک از آنان را محاسبه کند و ضمن ذخیره آن در ستون چهارم
همان آرایه آن را به همراه شماره کار مربوطه چاپ نماید.
======================================================================"""
#        Make List
#-----------------------------------
information = []
for temp in range(1,21):
    person_info = []
    print("Please enter information for person",temp)
    id = int(input("ID ==>"))
    person_info.append(id)
    hour_in_a_week = int(input("Hour in a week ==>"))
    person_info.append(hour_in_a_week)
    price_for_one_hour = int(input("Price for one hour ==>"))
    person_info.append(price_for_one_hour)
    information.append(person_info)


#           Income for every person
#------------------------------------------
for temp in range(len(information)):
    income = information[temp][1] * information[temp][2]
    information[temp].append(income)

#            Show income for everyone
#-----------------------------------------
for temp in range(len(information)):
    print("="*40,"\nID:",information[temp][0],"===>","Income",\
          information[temp][3])

