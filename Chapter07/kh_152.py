"""===================================================================
با فرض اینکه یک ماتریس ۷*۶ قبلاً ذخیره شده است، منطق برنامه ای را طراحی
کنید که با دریافت I و J که نشانگر شماره های سطرهای ماتریس هستند این دو
سطر را با هم جابجا کنید.
==================================================================="""

#             Make matrix A
#------------------------------------

matrix_A = []
for i in range(1,7):
    row = []
    for j in range(1,8):
        row.append(i*j)
        print(i*j,end="\t")
    matrix_A.append(row)
    print()


#           recieve I and J
#--------------------------------------
i = int(input("Please enter I:"))
j = int(input("Please enter J:"))
for temp in range(len(matrix_A[i])):
    x = matrix_A[i][temp]
    matrix_A[i][temp] = matrix_A[j][temp]
    matrix_A[j][temp] = x

for i in range(len(matrix_A)):
    for j in range(len(matrix_A[i])):
        print(matrix_A[i][j],end="\t")
    print()
