"""===============================================================
منطق برنامه ای را طراحی کنید که ۲۴ عدد را در ورودی بخواند و به صورت
ستونی در یک ماتریس ۴*۶ ذخیره کند. سپس عناصر ماتریس را نمایش دهد.
==============================================================="""

#           Make Matrix
#---------------------------------------
matrix = []
for i in range(6):
    row = []
    for j in range(4):
        print("Please enter obj in row",i+1,"and column",j+1)
        num = int(input("==> "))
        row.append(num)
    matrix.append(row)

#            Show obj in matrix
#-----------------------------------------
for i in range(len(matrix)):
    for j in range(len(matrix[i])):
        print(matrix[i][j],end="\t")
    print()

