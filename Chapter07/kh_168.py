"""================================================================
با فرض اینکه اطلاعات ۲۵ نفر دانشجو به گونه‌ای که در خواسته ۱۶۷ مطرح شد
در دو آرایه به هم مرتبط NAME ,INFO ذخیره شده است، منطق برنامه‌ای را
طراحی کنید که نام و نام خانوادگی و نمرات دروس گذرانده شده در ترم جاری
دانشجویان را از دو آرایه ذکر شده استخراج و چاپ کند.
================================================================"""

#           167
#--------------------------------------------
# name = []
# info = []
# for temp in range(1,26):
#     print("="*30,"\nPlease enter namer for student",temp)
#     nam = input("==>")
#     name.append(nam)
#     row = []
#     print("Please enter info about", name[-1])
#     id = int(input("ID ==> "))
#     row.append(id)
#     for count in range(1,6):
#         print("Please enter info lesson",count,"for",name[-1])
#         code = int(input("Code ==> "))
#         row.append(code)
#         mark = float(input("Mark ==> "))
#         row.append(mark)
#     info.append(row)
#

name = ['Mohammad Rostami', 'Mohammad Zamani']
info = [[1, 1, 20.0, 2, 20.0, 3, 20.0, 4, 20.0, 5, 20.0], [2, 1, 12.0, 2, 13.0, 3, 14.0, 4, 15.0, 5, 16.0]]

#              168
#--------------------------------
for i in range(len(name)):
    print(name[i])
    print("-"*20)
    for j in range(2,11,2):
        print(info[i][j])

