"""===================================================================
با فرض اینکه دو ماتریس ۶*۵ و هم مرتبه قبلاً ذخیره شده اند، منطق برنامه‌ای
را طراحی کنید که ماتریس تفاضل این دو ماتریس را بسازد و ضمن ساختن آن
عناصر آن را به صورت ماتریس گونه چاپ نماید.
==================================================================="""

#             Make matrix A
#------------------------------------

matrix_A = []
for i in range(1,6):
    row = []
    for j in range(1,7):
        row.append(i*j)
    matrix_A.append(row)



#         Make Matrix B
#------------------------------------------
matrix_B = []
for i in range(1,6):
    row = []
    for j in range(1,7):
        row.append(3*i*j)
    matrix_B.append(row)



#       Make matrix C=A-B  and show that
#----------------------------------------------
matrix_C = []
for i in range(len(matrix_A)):
    row = []
    for j in range(len(matrix_A[i])):
        k = matrix_B[i][j] - matrix_A[i][j]
        row.append(k)
        print(k,end="\t")
    matrix_C.append(row)
    print()
