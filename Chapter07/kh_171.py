"""=============================================================
منطق برنامه ای را طراحی کنید که یک ماتریس ۸*۵ به نام P را به طوری
بسازد و چاپ نماید که هر یک از درایه های سطر اول و آخر ماتریس ۸ و
سایر درایه ها ۵ باشند.
============================================================="""

P = []
for i in range(5):
    row = []
    for j in range(8):
        if i == 0 or i==4:
            row.append(8)
            print(8,end="\t")
        else:
            row.append(5)
            print(5,end="\t")
    print()
    P.append(row)
