"""==========================================================
با فرض اینکه دو ماتریس A و B با مرتبه های 5*6 و 7*5 قبلاً ذخیره
شده اند، منطق برنامه‌ای را طراحی کنید که از ضرب دو ماتریس،
ماتریس C را بسازد.
=========================================================="""

#      Make matrix A
#---------------------------------
matrix_A = []
for i in range(1,7):
    row = []
    for j in range(1,6):
        row.append(i*j)
        print(i*j,end="\t")
    print()
    matrix_A.append(row)

print("="*30)

#     Make matrix B
#------------------------------------
matrix_B = []
for i in range(1,6):
    row = []
    for j in range(1,8):
        row.append(i+j)
        print(i+j,end="\t")
    matrix_B.append(row)
    print()
print("="*30)


#     Make matrix C=A*B
#-------------------------------------
matrix_C = []
for i in range(len(matrix_A)):
    row = []
    for j in range(len(matrix_B[0])):
        sum = 0
        for k in range(len(matrix_A[i])):
            sum = sum + (matrix_A[i][k]*matrix_B[k][j])
        row.append(sum)
        print(sum,end="\t")
    matrix_C.append(row)
    print()






