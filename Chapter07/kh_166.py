"""==================================================================
با فرض اینکه دو ماتریس Aو‌B هر کدام با مرتبه 8*7 قبلاً ذخیره شده‌اند، منطق
برنامه‌ای را طراحی کنید که ماتریس C را با استفاده از بزرگترین عنصر متناظر
دو ماتریس A,B و ماتریس D را با استفاده از کوچکترین عناصر متناظر دو
ماتریس A,B بسازد و در صورت مساوی بودن عناصر متناظر، عنصر مساوی را به
هر دو ماتریس C , D اضافه کند.
=================================================================="""

#        Make matrix A
#----------------------------------
A = []
for i in range(1,8):
    row = []
    for j in range(1,9):
        row.append(i+j)
        print(i+j,end="\t")
    A.append(row)
    print()

print("="*30)


#          Make Matrix B
#-----------------------------------
B = []
for i in range(1,8):
    row = []
    for j in range(1,9):
        row.append(i*j)
        print(i*j,end="\t")
    B.append(row)
    print()

print("="*30)

#          Make Matrix C,D
#-------------------------------------
C = []
D = []
for i in range(len(A)):
    row_c = []
    row_d = []
    for j in range(len(A[i])):
        if A[i][j] > B[i][j]:
            row_c.append(A[i][j])
            row_d.append(B[i][j])
        elif A[i][j] == B[i][j]:
            row_c.append(A[i][j])
            row_d.append(A[i][j])
        else:
            row_d.append(A[i][j])
            row_c.append(B[i][j])
    C.append(row_c)
    D.append(row_d)

#         show matrix C
#--------------------------------------
for i in range(len(C)):
    for j in range(len(C[i])):
        print(C[i][j],end="\t")
    print()

print("-"*30)
#         show matrix D
#--------------------------------------
for i in range(len(D)):
    for j in range(len(D[i])):
        print(D[i][j],end="\t")
    print()