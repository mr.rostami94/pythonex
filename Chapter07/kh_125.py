"""================================================================
منطق برنامه را طراحی کنید که سال تولد هر یک از ۶۰ نفر کارمندان شرکتی
را دریافت کند و در آرایه ای ذخیره کند و ضمن آن سن هر یک را بر اساس
سال ۱۳۹۸ محاسبه و در آرایه دیگری ذخیره کند. سرانجام جوان ترین و سن
مسن ترین کارمند را مشخص کند و چاپ نماید.
================================================================"""

birth = []
age = []
for count in range(1,61):
    print("Please enter birthday for ",count," staff")
    birthday = int(input("==>"))
    staff_age = 1398 - birthday
    birth.append(birthday)
    age.append(staff_age)

max = age[0]
min = age[0]
for i in age:
    if i > max:
        max = i
    if i < min:
        min = i

print("="*20,"\nMax: ",max,"\tMin: ",min)
