"""=============================================================
منطق برنامه ای را طراحی کنید که ۷۰ عدد را از ورودی بگیرد و در آنها
را در آرایه ای ذخیره کند
میانگین آنها را مشخص
تعداد اعداد بزرگتر از میانگین
تعداد اعداد کوچکتر از میانگین
و تعداد اعداد مساوی با میانگین را مشخص کند
============================================================="""

#             make list
#------------------------------------
mylist = []
sigma = 0
for temp in range(1,71):
    print("Please enter",temp,"number")
    number = int(input("==>"))
    sigma += number
    mylist.append(number)

#     Average
#---------------------------

average = sigma/len(mylist)
print("="*20,"\nAverage:",average)

#         compare with average
#------------------------------------
less = 0
bigger = 0
equal = 0
for temp in mylist:
    if temp > average:
        bigger += 1
    elif temp < average:
        less += 1
    else:
        equal += 1

print("="*40,"\nLess: ",less,"\tEqual:",equal,"\tBigger: ",bigger)

#    END PROGRAM