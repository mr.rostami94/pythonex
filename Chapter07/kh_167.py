"""==================================================================
منطق برنامه‌ای را طراحی کنید که نام و نام خانوادگی هر ۲۵ دانشجو را بگیرد
و در آرایه NAME و شماره دانشجویی و کد درس و نمره درس مربوط به هر یک از
پنج درس گذرانده شده در ترم جاری را در آرایه INFO ذخیره نماید به گونه‌آی
که در در آرایه INFO در ستون اول شماره دانشجویی و در ستونهای ۲و۴و۶و۸و۱۰
کد درسها و در ستونهای ۳و۵و۷و۹و۱۱ نمره درس ها ذخیره شوند.
=================================================================="""

name = []
info = []
for temp in range(1,26):
    print("="*30,"\nPlease enter namer for student",temp)
    nam = input("==>")
    name.append(nam)
    row = []
    print("Please enter info about", name[-1])
    id = int(input("ID ==> "))
    row.append(id)
    for count in range(1,6):
        print("Please enter info lesson",count,"for",name[-1])
        code = int(input("Code ==> "))
        row.append(code)
        mark = float(input("Mark ==> "))
        row.append(mark)
    info.append(row)
print(name)
print(info)