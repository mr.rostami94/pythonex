"""======================================================================
با فرض اینکه شماره دانشجویی، تعداد واحدهای گذرانده و معدل هر یک از ۲۰ نفر
دانشجویان کلاسی قبلاً به ترتیب در ستونهای ۱ و ۲ و ۳ آرایه A ذخیره شدند، منطق
برنامه ای را طراحی کنید که ضمن چاپ شماره دانشجویی و معدل هر دانشجو،بالاترین
معدل را مشخص و به همراه شماره دانشجویی دارنده چنین معدلی چاپ کند.
======================================================================"""

#     Make Array A
#-----------------------------------

A = []
for temp in range(1,21):
    row = []
    print("Please enter information for student",temp)
    id = int(input("ID ==> "))
    row.append(id)
    vahed = int(input("Vahed ==> "))
    row.append(vahed)
    avg = float(input("Average ==> "))
    row.append(avg)
    A.append(row)



#      Show id and average
#--------------------------------------
for i in range(len(A)):
    print("="*30,"\nid",A[i][0],"\t===>\t","Average =",A[i][2])

#      Max Average
#---------------------------------------
max = A[0][2]
id = A[0][0]
for i in range(len(A)):
    if A[i][2] > max:
        max = A[i][2]
        id = A[i][0]

print("="*30,"\nMax =",max,"for ID",id)
