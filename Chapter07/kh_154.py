"""==============================================================
منطق برنامه ای را طراحی کنید که عناصر روی قطر اصلی یک ماتریس ۶*۶
به نام A را که قبلاً ذخیره گردیده است به ۲ تبدیل نماید و بقیه عناصر
را با صفر جایگزین کند و پس از تعویض هر درایه آن را در قالب ماتریس
نمایش دهد.
=============================================================="""

#         Make matrix A
#---------------------------------
matrix_A = []
for i in range(1,7):
    row = []
    for j in range(1,7):
        row.append(i+j)
        print(i+j,end="\t")
    matrix_A.append(row)
    print()



#         replace obj
#---------------------------------

for i in range(len(matrix_A)):
    for j in range(len(matrix_A[i])):
        if i==j:
            matrix_A[i][j] = 2
        else:
            matrix_A[i][j] = 0

#         Show matrix A
#----------------------------------
print("="*30)
for i in range(len(matrix_A)):
    for j in range(len(matrix_A[i])):
        print(matrix_A[i][j],end="\t")
    print()

