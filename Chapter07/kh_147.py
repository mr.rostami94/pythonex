"""===============================================================
منطق برنامه ای را طراحی نمایید که ۳۵ عدد را بخواند و به صورت سطری
در یک ماتریس ۷*۵ ذخیره کند و سپس عناصر ماتریس را نمایش دهد.
==============================================================="""

#              Make matrix
#--------------------------------------
matrix = []
for i in range(5):
    row = []
    for j in range(7):
        print("Please enter obj",i+1,"row and",j+1,"column")
        num = int(input("==>"))
        row.append(num)
    matrix.append(row)

#              Show obj in matrix
#--------------------------------------
for i in range(len(matrix)):
    for j in range(len(matrix[i])):
        print(matrix[i][j],end=" ")
    print()