"""================================================================
با فرض اینکه شماره دانشجویی، تعداد واحدهای گذرانده و معدل هر ۲۵ دانشجو
قبلاً به ترتیب در ستونهای ۱و۲و۳ ارایه B ذخیره شده اند، منطق برنامه ای را
طراحی کنید که ضمن چاپ شماره دانشجویی و معدل هر یک از دانشجویان مشروط
تعداد دانشجویان مشروط را نیز مشخص و چاپ کند.
================================================================"""

#     Make Array B
#-----------------------------------
B = []
for temp in range(1,26):
    row = []
    print("Please enter information for student",temp)
    id = int(input("ID ==> "))
    row.append(id)
    vahed =  int(input("Vahed ==> "))
    row.append(vahed)
    avg = float(input("Average ==> "))
    row.append(avg)
    B.append(row)


#           Show/Numbers Student Faid
#----------------------------------------------------

numbers = 0
for temp in range(len(B)):
    if B[temp][2] < 12:
        print("-"*30,"\nID",B[temp][0],"\t==>\t","Average =",B[temp][2],"(FAID)")
        numbers += 1
print("="*30,"\nNumbers:",numbers)