"""=================================================================
منطق برنامه ای را طراحی کنید که عناصر یک ماتریس ۵*۴ را بخواند و در
 آرایه A ذخیره کند. سپس ضمن خواندن عناصر ماتریس ۵*۴ دیگری و ذخیره
کردن آن در آرایه B ماتریس مجموع آنها را بسازد و به صورت ماتریس
چاپ کند.
================================================================="""

#             Make matrix A
#------------------------------------

matrix_A = []
for i in range(1,5):
    row = []
    for j in range(1,6):
        row.append(i*j)
    matrix_A.append(row)

#           Make array A
#----------------------------------------
A = []
for i in range(len(matrix_A)):
    for j in range(len(matrix_A[i])):
        A.append(matrix_A[i][j])
#         Make Matrix B
#------------------------------------------
matrix_B = []
for i in range(1,5):
    row = []
    for j in range(1,6):
        row.append(3*i*j)
    matrix_B.append(row)

#         Reab obj in Matrix B and Make Matrix C
#-----------------------------------------------

matrix_C = []
for i in range(len(matrix_B)):
    row = []
    for j in range(len(matrix_B[i])):
        k = matrix_A[i][j] + matrix_B[i][j]
        row.append(k)
        print(k,end="\t")
    matrix_C.append(row)
    print()