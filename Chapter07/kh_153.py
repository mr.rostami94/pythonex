"""====================================================================
با فرض اینکه عناصر یک ماتریس ۸*۵ قبلاً ذخیره شده است. منطق برنامه‌ای را
طراحی کنید که با دریافت k و d که نشانگر ستونهای ماتریس هستند آن ستون
را با هم جابجا کنید.
===================================================================="""

#     make matrix A
#---------------------------
matrix_A = []
for i in range(1,6):
    row = []
    for j in range(1,9):
        row.append(i*j)
        print(i*j, end="\t")
    matrix_A.append(row)
    print()

#        Take K and D
#----------------------------
k = int(input("Please enter K:"))
d = int(input("Please enter D:"))

#        Replace K with D column
#-----------------------------------------
for temp in range(len(matrix_A)):
    x = matrix_A[temp][k]
    matrix_A[temp][k] = matrix_A[temp][d]
    matrix_A[temp][d] = x

#         Show matrix A
#-------------------------------------
for i in range(len(matrix_A)):
    for j in range(len(matrix_A[i])):
        print(matrix_A[i][j],end="\t")
    print()


