"""===========================================================
آرایه ای را طراحی کنید که ۱۰۰ عدد را بخواند در آرایه ای ذخیره
کند و سپس بزرگترین را به همراه اندیس آن بازگرداند.
==========================================================="""
mylist = []
for count in range(1,101):
    print("Please enter number ",count)
    num = float(input("==>"))
    mylist.append(num)
max = mylist[0]
for i in range(len(mylist)):
    if mylist[i] > max:
        max = mylist[i]
        index = i

print("="*30,"\nMax: ",max,"\tIndex: ",index)
