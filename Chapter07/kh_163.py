"""==================================================================
منطق برنامه‌ای را طراحی کنید که عدد طبیعی N را در ورودی دریافت کند و
سپس یک ماتریس مربعی N*N به نام P بسازد به گونه‌ای که عناصر قطرهای اصلی
و فرعی آن همگی صفر و سایر عناصر آن یک باشند.
=================================================================="""

#         Take N
#-------------------------------
n = int(input("Please enter N:"))

#          Make matrix P
#----------------------------------
P=[]
for i in range(n):
    row = []
    for j in range(n):
        if i==j or i+j == n-1:
            row.append(0)
            print(0,end="\t")
        else:
            row.append(1)
            print(1,end="\t")
    P.append(row)
    print()