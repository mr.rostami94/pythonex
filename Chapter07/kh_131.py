"""==========================================================
منطق برنامه ای را طراحی کنید که ۴۰ عدد را بخواند و در آرایه ای ذخیره کند
سپس مجموع عناصر ذخیره شده در محل های حاوی اندیس های فرد و
مجموع عناصر ذخیره شده در محل های حاوی اندیس های زوج را به صورت جداگانه
محاسبه کند و نسبت آنها را به یکدیگر مشخص کند و
مجموع ها و نسبت حاصل را چاپ کند
=========================================================="""
mylist = []
for count in range(1,41):
    print("Please enter number",count)
    num = int(input("==>"))
    mylist.append(num)

sigma_odd = 0
sigma_even = 0
for i in range(len(mylist)):
    if i % 2 == 0:
        sigma_even += mylist[i]
    else:
        sigma_odd += mylist[i]

n = sigma_odd/sigma_even
print("="*45,"\nSigma Odd: ",sigma_odd,"\tSigma Even: ",sigma_even \
      ,"\tNesbat: ",n)