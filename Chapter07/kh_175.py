"""===============================================================
منطق برنامه‌ای را طراحی کنید که با دریافت عناصر یک ماتریس ۲*۲ به
نام B بسازد و سپس دترمینان آن را محاسبه و چاپ کند.
==============================================================="""
B = []
for i in range(1,3):
    row = []
    for j in range(1,3):
        print("Please enter obj in row",i,"and column",j)
        obj = int(input("==> "))
        row.append(obj)
    B.append(row)

print("="*30)
#      Show B
#-----------------------------
for i in range(len(B)):
    for j in range(len(B[i])):
        print(B[i][j],end="\t")
    print()


#         Determin
#------------------------------
sigma1 = 1
sigma2 = 1
for i in range(len(B)):
    for j in range(len(B[i])):
        if i==j:
            sigma1 = sigma1 * (B[i][j])
        if i+j == len(B)-1:
            sigma2 = sigma2 * B[i][j]

result = sigma1 - sigma2
print("="*30,"\nDetermin: ",result)