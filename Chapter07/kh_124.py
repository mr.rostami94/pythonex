"""=====================================================================
منطق برنامه ذخیره کننده طول قد ۱۱ نفر بازیکنان یک تیم فوتبال در یک آرایه
و تعیین کوتاهترین بازیکن
====================================================================="""

mylist = []
count = 1
while count <= 11:
    print("Please enter height for ",count," athlete")
    height = float(input("==> "))
    mylist.append(height)
    count+=1

min = mylist[0]
for i in mylist:
    if i < min:
        min = i

print("="*20,"\nMin = ",min," cm")