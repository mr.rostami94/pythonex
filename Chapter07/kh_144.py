"""==============================================================
منطق برنامه ای را طراحی کنید که ۳۰ عدد را بخواند و اگر عدد بر ۷
بخش پذیر بود آن عدد را در یک آرایه و توان ۳ آن را در آرایه دیگری
ذخیره کند
تعداد این اعداد
محتویات آرایه حاوی این اعداد
ارایه دربرگیرنده توان ۳ این اعداد را چاپ کند
=============================================================="""


#         MAKE LIST
#-------------------------------------
mylist_number = []
mylist_cube_number = []
count_number = 0
for temp in range(1,31):
    print("Please enter",temp,"number")
    number = int(input("==>"))
    if number % 7 == 0:
        count_number += 1
        mylist_number.append(number)
        cube_of_number = number ** 3
        mylist_cube_number.append(cube_of_number)

print("="*40,"\nCount Number:",count_number,\
      "\nNumbers:",mylist_number,\
      "\nCube of Numbers:",mylist_cube_number)