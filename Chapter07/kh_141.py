"""==============================================================
منطق برنامه ای را طراحی کنید که نمرات آزمون ۴۰ دانش آموز را بخواند
و در آرایه ای ذخیره کند و میانگین نمرات را محاسبه کند. سپس اختلاف
هر نمره را با میانگین مشخص و چاپ کند.
=============================================================="""

grade = []
for temp in range(1,41):
    print("Please enter",temp,"grade")
    mark = float(input("==>"))
    grade.append(mark)

sigma = 0
for temp in grade:
    sigma += temp
average = sigma/len(grade)

print("="*20,"\nAverage = ",average)
print("="*20)

for temp in grade:
    k = abs(temp - average)
    print(temp,"==>",k)