"""==================================================================
با فرض اینکه شماره دانشجویی، تعداد واحدهای گذرانده و معدل هر ۳۰ دانشجو
قبلاً به ترتیب در ستونهای ۱و۲و۳ آرایه A ذخیره شده است؛ منطق برنامه ای را
طراحی کنید که شماره دانشجویی و تعداد واحدهای دانشجویی که کمتر از ۶۰ واحد
گذرانده اند را چاپ نماید و ضمن انجام این کار دانشجویی که کمترین تعداد
واحد را گذرانده است مشخص نماید و شماره دانشجویی و تعداد واحدهای او را
نیز چاپ کنید.
=================================================================="""

#       Make Array A
#----------------------------------

A = []
for temp in range(1,36):
    row = []
    print("Please enter information for student",temp)
    id = int(input("ID ==> "))
    row.append(id)
    unit = int(input("Unit ==> "))
    row.append(unit)
    avg = float(input("Average ==> "))
    row.append(avg)
    A.append(row)

#        less 60 unit and min unit
#--------------------------------------

min_unit = A[0][1]
min_id = A[0][0]
for temp in range(len(A)):
    if A[temp][1] < 60:
        print("-"*30,"\nID",A[temp][0],"\t==>\t","Unit=",A[temp][1])
    if A[temp][1] < min_unit:
        min_unit = A[temp][1]
        min_id = A[temp][0]

print("="*30,"\nMin Unit = ",min_unit,"\t==>\t","ID=",min_id)

