"""============================================================
منطق برنامه ای را طراحی کنید که اعداد فرد دو رقمی را در یک آرایه
و مربع آنها را در آرایه دیگری ذخیره نماید و میانگین اعداد فرد و
میانگین مربعات آنها را محاسبه و چاپ نماید.
============================================================"""


#         Make list
#-------------------------------
mylist_odd = []
mylist_square_odd = []
sigma_odd = 0
sigma_square_odd = 0
for temp in range(10,100):
    if temp % 2 != 0:
        sigma_odd += temp
        mylist_odd.append(temp)
        square_temp = temp**2
        sigma_square_odd += square_temp
        mylist_square_odd.append(square_temp)


#          Average
#--------------------------------
average_for_odd = sigma_odd/len(mylist_odd)
average_for_square_odd = sigma_square_odd/len(mylist_square_odd)

print("="*40,"\nAverage for odd:",average_for_odd,\
      "\nAverage for square odd:",average_for_square_odd)
